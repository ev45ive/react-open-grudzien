
import React, { useContext } from 'react'
import { UserContext } from '../contexts/UserContext'

export const UserWidget = () => {
  const {
    loggedIn, user, login, logout
  } = useContext(UserContext)


  return (
    <div>
      {loggedIn ?
        <>
          Welcome, {user?.display_name}
          <span onClick={() => logout()}> LogOut</span>
        </> :
        <>
          Welcome Guest,
          <span onClick={() => login()}> LogIn</span>
        </>}
    </div>
  )
}
