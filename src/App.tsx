import React, { useState } from 'react';
import logo from './logo.svg';
import 'bootstrap/dist/css/bootstrap.css';
import './App.css';
import PlaylistsView from './playlists/containers/PlaylistsView';
import { AlbumSearch } from './search/containers/AlbumSearch';

import { NavLink, Redirect, Route, Switch } from 'react-router-dom'
import UserProfile from './users/containers/UserProfile';
import { AlbumSearchReduce } from './search/containers/AlbumSearchReduce';
import { UserContext } from './core/contexts/UserContext';
import { UserWidget } from './core/components/UserWidget';
import { AlbumSearchRedux } from './search/containers/AlbumSearchRedux';
import { AlbumDetails } from './search/containers/AlbumDetails';
import PlaylistsViewRedux, { PlaylistsViewContainer } from './playlists/containers/PlaylistsViewRedux';
import PlaylistsHooksRedux from './playlists/containers/PlaylistsHooksRedux';

const App = () => {
  const [menuOpen, setMenuOpen] = useState(false)
  return (
    <div>
      <nav className="navbar navbar-expand-sm navbar-dark bg-dark mb-3">
        <div className="container">
          <NavLink className="navbar-brand" to="/">Music App</NavLink>
          <button className="navbar-toggler" type="button" onClick={() => setMenuOpen(!menuOpen)}>
            <span className="navbar-toggler-icon"></span>
          </button>

          <div className={`collapse navbar-collapse ${menuOpen ? 'show' : ''}`}>
            <ul className="navbar-nav">
              <li className="nav-item">
                <NavLink className="nav-link" activeClassName="placki active" to="/search">Search</NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" to="/playlists">Playlists</NavLink>
              </li>
            </ul>

            <div className="ms-auto text-white navbar-text">
              <UserWidget />
            </div>

          </div>
        </div>
      </nav>


      <div className="container">
        <div className="row">
          <div className="col">

            <Switch>
              <Redirect path="/" exact={true} to="/playlists" />
              {/* ==== */}
              <Route path="/playlists" component={PlaylistsHooksRedux} />
              <Route path="/search" component={AlbumSearchRedux} />
              <Route path="/albums/details/:album_id" component={AlbumDetails} />
              <Route path="/user" component={UserProfile} />
              {/* ==== */}
              {/* <Route path="*" component={PageNotFound}/> */}
              <Route path="*" render={() => <h1>Page Not Found</h1>} />
            </Switch>

          </div>
        </div>
      </div>

    </div>
  );
}

export default App;
