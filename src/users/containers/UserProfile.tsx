
import React, { useEffect, useState } from 'react'
import { UserProfile as User } from '../../core/model/User'
import { userService } from '../../core/services'

interface Props { }

export const UserProfile = (props: Props) => {
  const [user, setUser] = useState<User | null>(null)

  // const fetchData = async () => {
  //   const res = await userService.getUserProfile()
  //   setUser(res.data)
  // }
  // useEffect(() => { fetchData() }, [])

  useEffect(() => { // IIFC 
    (async () => {
      setUser(await userService.getUserProfile())
    })()
  }, [])

  return (
    <div>
      <h3>User Profile</h3>

      {user && <dl>
        <dt>Name:</dt>
        <dd>{user.display_name}</dd>

        <dt>Email:</dt>
        <dd>{user.email}</dd>

        <dt>Followers:</dt>
        <dd>{user.followers.total}</dd>
      </dl>}

    </div>
  )
}

export default UserProfile