
// tsrcc
import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { Playlist } from '../../core/model/Playlist'
import { AppState } from '../../store'
import { PlaylistDetails } from '../components/PlaylistDetails'
import PlaylistForm from '../components/PlaylistForm'
import PlaylistsList, { PlaylistsListContainer } from '../components/PlaylistsList'
import * as fromReducer from '../../reducers/Playlists.reducer'
import { connect } from 'react-redux'
import { ThunkDispatch } from 'redux-thunk'

// Higher Order Component = HOC
const PlaylistFormWithRouter = withRouter(PlaylistForm)

interface Props {
  selected?: Playlist,
  onSelect(id: Playlist['id']): void,
  onSave(draft: Playlist): void,
  onCancel(): void,
  onLoadData(): void
}

interface State {
  mode: 'details' | 'edit',
}

export default class PlaylistsViewRedux extends Component<Props, State> {
  state: State = {
    mode: 'details',
  }

  componentDidMount() {
    this.props.onLoadData()
  }

  select = (selectedId: Playlist['id']) => { }

  edit = () => { this.setState({ mode: 'edit' }) }
  cancel = () => { this.setState({ mode: 'details' }); this.props.onCancel() }
  save = (draft: Playlist) => { }

  render() {
    return (
      <div>
        <div className="row">
          <div className="col">
            <PlaylistsListContainer />
          </div>
          <div className="col">
            {this.props.selected && <>
              {this.state.mode === 'details' &&
                <PlaylistDetails playlist={this.props.selected} onEdit={this.edit} />}

              {this.state.mode === 'edit' &&
                <PlaylistFormWithRouter playlist={this.props.selected} onCancel={this.cancel} onSave={this.save} />}
            </>}
            {!this.props.selected && <p>Please select playlist</p>}
          </div>
        </div>
      </div>
    )
  }
}

export const PlaylistsViewContainer = connect(
  (state: AppState) => ({
    selected: fromReducer.selectSelectedPlaylist(state),
  }),
  (dispatch: ThunkDispatch<any, any, any>) => ({
    onLoadData() { dispatch(fromReducer.loadMyPlaylists()) },
    // onSelected(id: Playlist['id']) { dispatch(fromReducer.select_playlist(id)) }
    onCancel() { dispatch(fromReducer.select_playlist(undefined)) },
    onSave(draft: Playlist) { dispatch(fromReducer.savePlaylist(draft)) }
  })
)(PlaylistsViewRedux)