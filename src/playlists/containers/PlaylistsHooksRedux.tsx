
import React, { ReactElement, useCallback, useEffect, useState } from 'react'
import { PlaylistDetails } from '../components/PlaylistDetails'
import PlaylistsList from '../components/PlaylistsList'
import * as fromReducer from '../../reducers/Playlists.reducer'
import { useDispatch, useSelector } from 'react-redux'
import PlaylistForm from '../components/PlaylistForm'
import { Playlist } from '../../core/model/Playlist'
import { withRouter } from 'react-router-dom'

interface Props { }

export default function PlaylistsHooksRedux({ }: Props): ReactElement {
  const playlists = useSelector(fromReducer.selectPlaylists)
  const selected = useSelector(fromReducer.selectSelectedPlaylist)
  const [mode, setMode] = useState<'details' | 'edit'>('details')
  const dispatch = useDispatch();
  const [query, setQuery] = useState('')

  useEffect(() => { dispatch(fromReducer.loadMyPlaylists()) }, [])

  // Use Callback for pure/memo children to prevent reload
  const select = useCallback((id: Playlist['id']) => { dispatch(fromReducer.select_playlist(id)) }, [])
  const cancel = useCallback(() => { dispatch(fromReducer.select_playlist(undefined)); setMode('details') }, [])
  const edit = useCallback(() => { setMode('edit') }, [])
  const save = useCallback((draft: Playlist) => { dispatch(fromReducer.savePlaylist(draft)) }, [])

  return (
    <div>
      <div>
        <div className="row">
          <div className="col">
            <input type="text" onChange={(e) => { setQuery(e.target.value) }} />
            {/* ====================================================================   v--- useCallback for PureComponent */}
            <PlaylistsList playlists={playlists} selected={selected?.id} onSelected={select} />
          </div>
          <div className="col">
            {selected && <>
              {mode === 'details' &&
                <PlaylistDetails playlist={selected} onEdit={edit} />}

              {mode === 'edit' &&
                <PlaylistFormWithRouter playlist={selected} onCancel={cancel} onSave={save} />}
            </>}
            {!selected && <p>Please select playlist</p>}
          </div>
        </div>
      </div>
    </div>
  )
}

const PlaylistFormWithRouter = withRouter(PlaylistForm)