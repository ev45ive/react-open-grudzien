
// tsrafc
import React from 'react'
import { Playlist } from '../../core/model/Playlist'
import styles from './PlaylistDetails.module.css'
import * as fromReducer from '../../reducers/Playlists.reducer'
import { connect } from 'react-redux'
import { AppState } from '../../store'

// Styled components
const cssjs = {
  'isPublicYes': {
    color: 'red'
  }
}
// cssjs.isPublicYes => Playlist_Details_isPublicYes_DNHALOI

console.log(styles)

interface Props {
  playlist: Playlist
  onEdit(): void
}

export const PlaylistDetails = React.memo(({ playlist, onEdit }: Props) => {

  return (
    <div title={playlist.name} data-id={'playlist_' + playlist.id}>
      <dl>
        <dt>Name:</dt>
        <dd>{playlist.name}</dd>
        <dt>Public</dt>

        <dd className={
          `${styles.isPublic} 
          ${playlist.public ? styles.isPublicYes : styles.isPublicNo}`

        }>{playlist.public ? 'Yes' : 'No'}</dd>
        <dt>Description:</dt>
        <dd>{playlist.description}</dd>
      </dl>

      <button className="btn btn-info" onClick={onEdit}>Edit</button>

    </div >
  )
}/* , propsAreEqual?: ((prevProps: Readonly<Props>, nextProps: Readonly<Props>) => boolean */)


// export const PlaylistsListContainer = connect(
//   (state: AppState) => ({
//     playlist: fromReducer.selectSelectedPlaylist(state),
//   }),
//   // (dispatch) => ({
//   //   onSelected(id: Playlist['id']) { dispatch(fromReducer.select_playlist(id)) }
//   // })
// )(PlaylistDetails)