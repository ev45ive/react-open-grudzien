import { Reducer } from "redux";

export const featureKey = 'counter'

export const reducer: Reducer = (state = 0, action: any) => {
  switch (action.type) {
    case 'INC': return state + action.payload
    case 'DEC': return state + action.payload
    default: return state;
  }
}

export const incCounter = (payload = 1) => ({ type: 'INC', payload })