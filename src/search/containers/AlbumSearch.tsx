
// tsrafc
import React, { useEffect } from 'react'
import { RouteComponentProps, useHistory, useLocation, useParams, useRouteMatch } from 'react-router-dom'
import { useSearchRequest } from '../../core/hooks/useSearchRequest'
import { albumSearch } from '../../core/services'
import { SearchForm } from '../components/SearchForm'
import { SearchResults } from '../components/SearchResults'

type Props = {} & RouteComponentProps<{ /* placki: string */ }>

export const AlbumSearch = (props: Props) => {
  // props.match.params.placki
  // const q = new URLSearchParams(props.location.search).get('q')
  // useLocation().search
  // useParams()
  // useHistory()
  // useRouteMatch()

  const {
    setQuery, error, loading, query, results
  } = useSearchRequest((query: string) => albumSearch.searchAlbums(query))

  const location = useLocation()
  useEffect(() => {
    const q = new URLSearchParams(location.search).get('q')
    setQuery(q || 'batman')
  }, [location.search])

  const history = useHistory()
  const search = (query: string) => {
    // history.push('/search?q=' + query)
    history.replace('/search?q=' + query)
  }

  return (
    <div>
      <div className="row">
        <div className="col">
          <SearchForm onSearch={search} query={query || ''} />
        </div>
      </div>

      <div className="row">
        <div className="col">
          {loading && <p className="alert alert-info">Loading...</p>}
          {error && <p className="alert alert-danger">{error}</p>}
          {query && <p onClick={() => search('')}>Results for "{query}"</p>}
          {results && <SearchResults results={results} />}
        </div>
      </div>
    </div>
  )
}
