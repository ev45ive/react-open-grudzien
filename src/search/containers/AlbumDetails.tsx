import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useParams, useRouteMatch } from 'react-router-dom'
import { Album } from '../../core/model/Album'
import { albumSearch } from '../../core/services'
import { AlbumCard } from '../components/AlbumCard'

export const AlbumDetails = () => {
  // const album_id = useRouteMatch<{ album_id: string }>().params.album_id
  const { album_id } = useParams<{ album_id: string }>()
  const [album, setAlbum] = useState<Album | null>(null)
  const [error, setError] = useState(null)

  useEffect(() => {
    const cancel = axios.CancelToken.source()
      ; (async () => {
        try {

          const album = await albumSearch.getAlbumById(album_id, cancel)
          setAlbum(album)
        } catch (error) { setError(error) }
      })()
    return () => cancel.cancel()
  }, [album_id])

  return (
    <div>
      {!album && <p className="alert alert-info">Loading...</p>}

      {album && <div className="row">
        <div className="col-3">
          <AlbumCard album={album} />
        </div>
        <div className="col">
          <dl>
            <dt>Name:</dt>
            <dd>{album.name}</dd>
            <dt>Artist:</dt>
            <dd>{album.artists[0].name}</dd>
          </dl>


          <div className="list-group">
            {album.tracks.items.map(track => <div className="list-group-item" key={track.id}>
              {track.name}
            </div>)}
          </div>
        </div>
      </div>}
    </div>
  )
}
