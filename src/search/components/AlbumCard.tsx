import React from 'react';
import { Link, useHistory } from 'react-router-dom';
import { Album } from '../../core/model/Album';

export interface Props {
  album: Album
}

export const AlbumCard = ({ album }: Props) => {
  const history = useHistory()

  return (
    <div className="card"
    >
      <img src={album.images[0].url} className="card-img-top" onClick={() => history.push('/albums/details/' + album.id)} />

      <div className="card-body">
        <h5 className="card-title">
          <Link to={'/albums/details/' + album.id}>{album.name}</Link>
        </h5>
      </div>
    </div>
  );
};
