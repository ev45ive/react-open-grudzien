# GIT 
cd ..
git clone https://bitbucket.org/ev45ive/react-open-grudzien.git react-open-grudzien
cd react-open-grudzien
npm i 
npm start

# Cwiczenia
git stash -u -m "moje zadanie" & git pull -f

# Instalacje
node -v
npm -v 
git --version

# Create react app
npm install -g create-react-app

# New Typescript project
cd ..
create-react-app react-open-grudzien --template typescript

# PRoxy
https://www.jhipster.tech/configuring-a-corporate-proxy/
https://create-react-app.dev/docs/proxying-api-requests-in-development/

# UI toolkits
npm install bootstrap@next
https://material-ui.com/
https://react.semantic-ui.com/
https://ant.design/
https://react-bootstrap.github.io/components/alerts

# Axios
npm i axios

# Oauth Client 

# React router
npm install react-router-dom @types/react-router-dom
yarn add react-router-dom @types/react-router-dom

# Map Reduce
https://static.googleusercontent.com/media/research.google.com/en//archive/mapreduce-osdi04.pdf

# Flux 
https://facebook.github.io/flux/


# Redux
npm i redux react-redux @types/react-redux
yarn add redux react-redux @types/react-redux

# Middleware
https://github.com/reduxjs/redux-thunk

npm install redux-thunk
yarn add redux-thunk

https://github.com/pburtchaell/redux-promise-middleware/blob/HEAD/docs/introduction.md

https://www.npmjs.com/package/redux-api-middleware#usage

https://redux-saga.js.org/

https://redux-observable.js.org/docs/basics/Epics.html


# Redux Logger / devtools
https://github.com/LogRocket/redux-logger

https://github.com/reduxjs/redux-devtools

https://github.com/zalmoxisus/redux-devtools-extension#1-with-redux


# Redux Toolkit
npm install @reduxjs/toolkit
yarn add @reduxjs/toolkit


# Reselect - Memoized Selectors
https://github.com/reduxjs/reselect
